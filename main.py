# Nita Bogdan, Grupa 244
from game import Game
from ai import min_max, alpha_beta
from ai import DEPTH_EASY, DEPTH_MEDIUM, DEPTH_HARD
from utils import get_valid_option
import numpy as np
from time import time

from time import time

input_files = [
    'input/input_01.txt',
]
algorithm_options = {
    'm': min_max,
    'a': alpha_beta,
}
difficulty_options = {
    'e': DEPTH_EASY,
    'm': DEPTH_MEDIUM,
    'h': DEPTH_HARD,
}
user_algorithm = None
user_difficulty = None


def human_controller(game: Game, player: str):
    print("Human turn!")
    start = time()
    state = game.current
    possible = game.expand(state, player)
    possible['exit'] = None

    res_action = get_valid_option(
        possible.keys(),
        f"Choose your action ({'/'.join(game.ACTIONS + ['exit'])}): "
    )
    if res_action == 'exit':
        for player in game.session.keys():
            score = game.estimate_score(game.current, player)
            print(f"Player {player} has score {score}")
        exit()
    game.current = possible[res_action]
    end = time()
    duration = (end-start) * 1000
    print(f"Thinking time: {duration:.2f}ms")

    pass


def computer_controller(game: Game, player: str):
    print("Computer turn!")
    start = time()
    algorithm = algorithm_options[user_algorithm]
    difficulty = difficulty_options[user_difficulty]

    possible = game.expand(game.current, player)

    res_state, res_score = algorithm(
        game=game,
        state=game.current,
        maximize=[player],
        depth=difficulty,
    )

    if res_state != None:
        res_action = None
        for action, state in possible.items():
            if (state.map == res_state.map).all():
                res_action = action
                break
        print(f"Computer choose action '{res_action}' with score {res_score}.")
        game.current = res_state
    else:
        print("Computer is blocked.")
    end = time()


if __name__ == "__main__":
    for file in input_files:
        game = Game(path_to_map=file)

        algo_opt = algorithm_options.keys()
        user_algorithm = get_valid_option(
            algo_opt,
            f"Choose AI algorithm ({'/'.join(algo_opt)}): "
        )

        diff_opt = difficulty_options.keys()
        user_difficulty = get_valid_option(
            diff_opt,
            f"Choose difficulty ({'/'.join(diff_opt)}): "
        )

        player_opt = game.PLAYERS
        human_id = get_valid_option(
            player_opt,
            f"Choose player ({'/'.join(player_opt)}): "
        )

        print('You chose to play with {}.'.format(human_id))
        game.assign(human_id, human_controller)

        computer_id = game.get_next_available_player()
        print('Computer will play with {}.'.format(computer_id))
        game.assign(computer_id, computer_controller)

        game.start()
