# Nita Bogdan, Grupa 244
from typing import List
from state import State
from game import Game

INF_PLUS = 999
INF_MINUS = -999
DEPTH_EASY = 5
DEPTH_MEDIUM = 15
DEPTH_HARD = 25


def min_max(
    game: Game,
    state: State,
    maximize: List[str],
    depth: int,
):
    if depth == 0 or game.is_final(state):
        score = game.estimate_score(state, state.last_turn)
        return (state, score)

    # calculez toate mutarile posibile din starea curenta
    player = game.get_next_player(state.last_turn)
    possible = game.expand(state, player)

    if player in maximize:
        chosen_state = None
        chosen_score = INF_MINUS
        for next_action, next_state in possible.items():
            res_state, res_score = min_max(
                game, next_state, maximize, depth-1
            )
            if res_score > chosen_score:
                chosen_score = res_score
                chosen_state = next_state
        return (chosen_state, chosen_score)

    else:
        chosen_state = None
        chosen_score = INF_PLUS
        for next_action, next_state in possible.items():
            res_state, res_score = min_max(
                game, next_state, maximize, depth-1
            )
            if res_score < chosen_score:
                chosen_score = res_score
                chosen_state = next_state
        return (chosen_state, chosen_score)


def alpha_beta(
    game: Game,
    state: State,
    maximize: List[str],
    depth: int,
    alpha: int = INF_MINUS,
    beta: int = INF_PLUS,

):
    if depth == 0 or game.is_final(state) or alpha >= beta:
        score = game.estimate_score(state, state.last_turn)
        return (state, score)

    player = game.get_next_player(state.last_turn)
    possible = game.expand(state, player)

    if player in maximize:
        chosen_state = None
        chosen_score = INF_MINUS

        for next_action, next_state in possible.items():
            res_state, res_score = alpha_beta(
                game, next_state, maximize, depth-1, alpha, beta,
            )
            if res_score > chosen_score:
                chosen_score = res_score
                chosen_state = next_state

            alpha = max(alpha, chosen_score)

            if alpha >= beta:
                print('pruning beta...')
                break
        return (chosen_state, chosen_score)

    else:
        chosen_state = None
        chosen_score = INF_PLUS

        for next_action, next_state in possible.items():
            res_state, res_score = alpha_beta(
                game, next_state, maximize, alpha, beta, depth-1
            )
            if res_score < chosen_score:
                chosen_score = res_score
                chosen_state = next_state

            alpha = min(alpha, chosen_score)

            if beta <= alpha:
                print('pruning alpha...')
                break
        return (chosen_state, chosen_score)
