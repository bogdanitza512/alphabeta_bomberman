# Nita Bogdan, Grupa 244
from utils import Map, Coordinate
from typing import List, Dict, Tuple, Callable
import numpy as np


class State:
    def __init__(
        self,
        map: Map = None,
        protections: Dict[str, int] = None,
        inactive_bombs: Dict[str, Coordinate] = None,
        k_counter: Dict[str, int] = None,
        last_turn: str = ''
    ):
        super().__init__()
        self.map = np.array(map)
        self.protections = protections
        self.inactive_bombs = inactive_bombs
        self.k_counter = k_counter
        self.last_turn = last_turn
        pass

    def __str__(self):
        return "{}\n".format(
            '\n'.join([''.join(line) for line in self.map.tolist()]),
        )
