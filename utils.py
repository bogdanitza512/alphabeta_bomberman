# Nita Bogdan, Grupa 244
from typing import List, Dict, Tuple, Callable

Map = List[List[str]]
Coordinate = Tuple[int, int]
Controller = Callable[['Game', str], 'Node']


def get_valid_option(options, prompt):
    while True:
        response = input(prompt)
        if response in options:
            return response
        else:
            print("Input is invalid. Please try again.")
