# Nita Bogdan, Grupa 244
from __future__ import annotations
from typing import List
from utils import Map


class Node:
    def __init__(
        self,
        state: Map = None,
        parent: Node = None,
        description: str = "",
    ):
        super().__init__()
        self.state = state
        self.parent = parent
        self.descripton = description
    pass

    def __str__(self):
        return "{}\n{}".format(
            self.descripton,
            '\n'.join([''.join(line) for line in self.state]),
        )

    def __str__(self):
        return "{}\n{}".format(
            self.descripton,
            '\n'.join([''.join(line) for line in self.state]),
        )
