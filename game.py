# Nita Bogdan, Grupa 244
from __future__ import annotations
from typing import List, Dict, Tuple, Callable, Set
import numpy as np
from node import Node
from utils import Map, Controller, Coordinate
from state import State
from copy import deepcopy
from time import time


class Game:
    WALL = '#'
    DEAD = 'X'
    EMPTY = ' '
    PROTECTION = "P"
    ACTIVE_BOMB = 'B'
    INACTIVE_BOMB = 'b'
    PLAYERS = ['1', '2']
    ACTIONS = ['w', 'a', 's', 'd', 'b']
    K_MAX = 5

    def __init__(self, path_to_map: str):
        super().__init__()
        self.turn: int = -1
        self.winner: str = None
        self.session: Dict[str, Controller] = dict()
        self.current: State = State(
            map=self.get_map_from_path(path_to_map),
            protections={player: 0 for player in Game.PLAYERS},
            inactive_bombs={player: None for player in Game.PLAYERS},
            k_counter={player: 0 for player in Game.PLAYERS},
            last_turn=Game.PLAYERS[self.turn]
        )

    def get_map_from_path(self, path: str) -> Map:
        with open(path, 'r') as file:
            return [list(line.strip()) for line in file.readlines()]
            pass
        pass

    def assign(self, id: str, controller: Controller):
        if id in Game.PLAYERS and id not in self.session.keys():
            self.session[id] = controller
        else:
            print("Player assigned to invalid symbol")
        pass

    def start(self):
        print(self.current)
        start = time()
        while not self.is_final(self.current):
            self.perform_player_turn()
            # self.perform_clean_up(self.current)
            print(self.current)
        print("Game over!")
        if self.winner != None:
            print(f"Player {self.winner} won!")
        else:
            print("No one won.")
        end = time()
        duration = (end-start) * 1000
        print(f"Game duration: {duration:.2f}ms")

    def perform_player_turn(self):
        player = self.get_next_turn()
        controller = self.session[player]
        controller(self, player)
        pass

    def get_next_turn(self) -> str:
        while True:
            self.turn = (self.turn + 1) % len(Game.PLAYERS)
            player = Game.PLAYERS[self.turn]
            state = self.current
            is_present = self.is_present(player, state)
            is_assigned = self.is_assigned(player, state)
            if is_present and is_assigned:
                return Game.PLAYERS[self.turn]

    def perform_clean_up(self, state: State):
        self.check_inactive_bombs(state)
        players, bombs = self.check_active_bombs(state)
        state.map[players] = Game.DEAD
        state.map[bombs] = Game.EMPTY
        pass

    def check_inactive_bombs(self, state: State):
        for player, bomb in state.inactive_bombs.items():
            if bomb != None and state.map[bomb] == Game.EMPTY:
                state.map[bomb] = Game.INACTIVE_BOMB

    def get_next_available_player(self) -> str:
        available = [
            player for player in Game.PLAYERS if player not in self.session.keys()
        ]
        return next(iter(available), None)

    def get_next_player(self, current: str):
        current_index = Game.PLAYERS.index(current)
        num_players = len(Game.PLAYERS)
        next_index = (current_index + 1) % num_players
        return Game.PLAYERS[next_index]

    def get_obstacles(self, player: str) -> List[str]:
        other_players = [other for other in Game.PLAYERS if other != player]
        return other_players + [
            Game.WALL, Game.ACTIVE_BOMB, Game.INACTIVE_BOMB, Game.DEAD
        ]

    def is_final(self, state: State):
        self.perform_clean_up(state)

        still_alive = np.array(Game.PLAYERS)
        still_alive = still_alive[np.isin(Game.PLAYERS, state.map)]

        if len(still_alive) == 0:
            self.winner = None
            return True
        elif len(still_alive) == 1:
            self.winner = still_alive[0]
            return True
        else:
            return False

    def check_active_bombs(self, state: State):
        players: Tuple[List[int], List[int]] = (list(), list())
        bombs: Tuple[List[int], List[int]] = (list(), list())

        bombs_x, bombs_y = np.where(state.map == Game.ACTIVE_BOMB)

        for player in Game.PLAYERS:
            obstacles = self.get_obstacles(player)
            player_x, player_y = np.where(state.map == player)

            x_intersection = np.intersect1d(bombs_x, player_x)
            for x in x_intersection:
                bomb_index = np.where(bombs_x == x)[0][0]
                player_index = np.where(player_x == x)[0][0]

                p_x = player_x[player_index]
                p_y = player_y[player_index]
                b_x = bombs_x[bomb_index]
                b_y = bombs_y[bomb_index]

                min_y = min(b_y, p_y)
                max_y = max(b_y, p_y)

                obstacle_found = False
                for y in range(min_y + 1, max_y):
                    if state.map[x, y] in obstacles:
                        obstacle_found = True
                        break
                if obstacle_found == False:
                    if state.protections[player] <= 0:
                        players[0].append(p_x)
                        players[1].append(p_y)
                    else:
                        state.protections[player] -= 1
                    bombs[0].append(b_x)
                    bombs[1].append(b_y)

            y_intersection = np.intersect1d(bombs_y, player_y)
            for y in y_intersection:
                bomb_index = np.where(bombs_y == y)[0][0]
                player_index = np.where(player_y == y)[0][0]

                p_x = player_x[player_index]
                p_y = player_y[player_index]
                b_x = bombs_x[bomb_index]
                b_y = bombs_y[bomb_index]

                min_x = min(b_x, p_x)
                max_x = max(b_x, p_x)

                obstacle_found = False
                for x in range(min_x+1, max_x):
                    if state.map[x, y] in obstacles:
                        obstacle_found = True
                        break
                if obstacle_found == False:
                    if state.protections[player] <= 0:
                        players[0].append(p_x)
                        players[1].append(p_y)
                    else:
                        state.protections[player] -= 1
                    bombs[0].append(b_x)
                    bombs[1].append(b_y)
        return (players, bombs)

    def expand(self, state: State, player: str):
        result: Dict[str, State] = dict()
        direction = {
            'w': (-1, 0),
            'a': (0, -1),
            's': (1, 0),
            'd': (0, 1),
        }

        for action in Game.ACTIONS:
            if action in direction.keys() and state.k_counter[player] < Game.K_MAX:
                result[action] = self.move(state, player, direction[action])
            elif action == 'b':
                if state.inactive_bombs[player] == None:
                    result[action] = self.place_bomb(state, player)
                else:
                    result[action] = self.activate_bomb(state, player)
                pass

        result = {
            action: state for action, state in result.items() if state is not None
        }
        return result

    def move(self, state: State, player: str, direction: Coordinate):
        result = None

        obstacles = self.get_obstacles(player)
        player_x, player_y = np.where(state.map == player)
        p_x = player_x[0]
        p_y = player_y[0]

        target = (p_x + direction[0], p_y + direction[1])

        obstacles_found = np.isin(state.map[target], obstacles).any()
        if not obstacles_found:
            result = deepcopy(state)
            if result.map[target] == Game.PROTECTION:
                result.protections[player] += 1
                result.map[target] = Game.EMPTY
            result.map[p_x, p_y], result.map[target] = \
                result.map[target], result.map[p_x, p_y]
            result.k_counter[player] += 1
            result.last_turn = player
        return result

    def place_bomb(self, state: State, player: str):
        result = None

        obstacles = self.get_obstacles(player)

        player_x, player_y = np.where(state.map == player)
        player_pos = (player_x[0], player_y[0])

        obstacles_found = np.isin(state.map[player_pos], obstacles).any()
        if not obstacles_found:
            result = deepcopy(state)
            result.inactive_bombs[player] = player_pos
            result.k_counter[player] = 0
            result.last_turn = player

        return result

    def activate_bomb(self, state: State, player: str):
        result = None

        bomb_pos = state.inactive_bombs[player]
        if bomb_pos != None:
            result = deepcopy(state)
            result.inactive_bombs[player] = None
            result.map[bomb_pos] = Game.ACTIVE_BOMB
            result.k_counter[player] = 0
            result.last_turn = player
        return result

    def is_present(self, player: str, state: State) -> bool:
        return (state.map == player).any()

    def is_assigned(self, player: str, state: State) -> bool:
        return player in self.session.keys()

    def estimate_score(self, state: State, target: str):
        score = 0

        players = [
            player for player in Game.PLAYERS
            if self.is_present(player, state)
        ]
        for player in players:
            player_x, player_y = np.where(state.map == player)
            p_x, p_y = player_x[0], player_y[0]

            bombs_x, bombs_y = np.where(state.map == Game.ACTIVE_BOMB)
            dist_x = min([abs(p_x-b_x) for b_x in bombs_x], default=99)
            dist_y = min([abs(p_y-b_y) for b_y in bombs_y], default=99)

            if player == target:
                score += dist_x + dist_y
            else:
                score -= dist_x + dist_y

            prots_x, prots_y = np.where(state.map == Game.PROTECTION)
            dist_x = min([abs(p_x-prot_x) for prot_x in prots_x], default=99)
            dist_y = min([abs(p_y-prot_y) for prot_y in prots_y], default=99)

            if player == target:
                score -= dist_x + dist_y
            else:
                score += dist_x + dist_y

        return score
